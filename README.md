# Software Studio 2020 Spring
## Assignment 01 Web Canvas




---

### How to use 
 | **Description**                                                                                                                                                                                                                                                                                                                                    | **Buttons**    |
 |:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |:-------------- |
 | Download the image(myImage.jpg) on the canvas.                                                                                                                                                                                                                                                                                                     | SAVE           |
 | Clear the canvas.                                                                                                                                                                                                                                                                                                                                  | CLEAR ALL      |
 | Go back to the previous step.                                                                                                                                                                                                                                                                                                                      | UNDO           |
 | Reverse undo.                                                                                                                                                                                                                                                                                                                                      | REDO           |
 | Select a image and upload such image to the canvas after clicking "UPLOAD".(Notice that everything on canvas will be covered by the new image)                                                                                                                                                                                                     | SELECT PHOTO   |
 | See description of "select photo".                                                                                                                                                                                                                                                                                                                 | UPLOAD         |
 | Erase things on the canvas.                                                                                                                                                                                                                                                                                                                        | ERASER         |
 | The cursor can draw things on the canvas.                                                                                                                                                                                                                                                                                                          | PEN            |
 | Type text on the canvas on the cursor where users click at. Click on the canvas to choose the position desired first, and then click any place again to show the text after typing input. The cursor will change to pen after user inserting a text.Notice that if size is too small, the text cannot be seen on the canvas.(recommand size >= 10) | TEXT           |
 | Draw a eclipse on the canvas.                                                                                                                                                                                                                                                                                                                      | CIRCLE         |
 | Draw a triangle on the canvas.                                                                                                                                                                                                                                                                                                                     | TRIANGLE       |
 | Draw a rectangle on the canvas. Rectangle and the above two shapes can be presented on the canvas with differnt line-width through the "size" function.  Also, user can easily chage color through the color selector.                                                                                                                             | RECTANGLE      |
 | After clicking "FILL SHAPE", all the shape that user drags will filled with color. Click again to cancel the fill shape funciton.(Notice that users cannot fill the shape in default. )                                                                                                                                                            | FILL SHAPE     |
 | Users can choose the size and linewidth of font, pen, eraser, circle, triangle and rectangle.  User should click the selceted icons to apply size value to it.                                                                                                                                                                                                                                                   | SIZE           |
 | Change the font of text.                                                                                                                                                                                                                                                                                                                           | FONT           |
 | Users can select the color of pen, circle, triangle and rectangle.                                                                                                                                                                                                                                                                                 | COLOR SELECTOR |
 
 ### Function
 
 #### The image below is the function buttons.
 
 ![](https://i.imgur.com/tHrbvxZ.png)
 
 ### Cursor
 
 ####  After choosing different function, the cursor will change to the coresponding image on the canvas.
 
 #### TEXT
 
![](https://i.imgur.com/c1VyCfo.png)

#### CIRCLE
![](https://i.imgur.com/FkFLcbe.png)

#### TRIANGLE
![](https://i.imgur.com/RMmQ0KE.png)

#### RECTANGLE
![](https://i.imgur.com/lyucP1Q.png)

#### PEN
![](https://i.imgur.com/jy4Glrc.png)

#### ERASER
![](https://i.imgur.com/UxAG9X2.png)








   
   

### Gitlab page link

  https://107062132.gitlab.io/AS_01_WebCanvas

### 備註

    網站是以13吋電腦螢幕做編排，若不是13吋螢幕，text可能會出現跑版。


