var x = 0;
var y = 0;


function download_img(el){
    console.log("save");
    var image = document.getElementById('drawable').toDataURL("image/jpg");
    el.href = image;
} // download


$(function() {
    var drawingtmp;
    var fill = 0;
    var scribble_canvasx = $("#drawable").offset().left;
    var scribble_canvasy = $("#drawable").offset().top;
    var scribble_last_mousex = scribble_last_mousey = 0;
    var scribble_mousex = scribble_mousey = 0;
    var scribble_mousedown = false;
    var drawMode = false,
        $drawable = $("#drawable"),
        ctx = $drawable[0].getContext("2d"),
        currentColor = "#000000",
        currentSize = 1,
        key = "save",
        textmode = 0,
        endX = 0,
        endY = 0,
        alreadyinput = 0,
        op = 2,
        historyImgs = [],
        step = 0,
        storage = window.localStorage[key],
        save = storage === "" || storage === undefined ? [] : JSON.parse(storage);
        historyImgs.push(document.getElementById('drawable').toDataURL());
    function toHex(n) {
        return (n < 16 ? "0" : "") + n.toString(16);
    }
    function mouseDown(color, size, x, y, isRecord) {
        ctx.beginPath();
       
        if(!op){ 
            ctx.globalCompositeOperation="destination-out";
            ctx.lineWidth = size;
            ctx.moveTo(x, y);
            drawMode = true;
            if (isRecord) {
                save.push([color, size, x, y]);
            }
        }
        else if(op === 1){
            ctx.globalCompositeOperation="source-over";
            ctx.strokeStyle = document.getElementById("mycolor").value;;
            console.log("color = " + color);
            ctx.lineWidth = size;
            ctx.moveTo(x, y);
            drawMode = true;
            if (isRecord) {
                save.push([color, size, x, y]);
            }
        }
        else{
            ctx.globalCompositeOperation="source-over";
        }
    }

    

    function mouseMove(x, y, isRecord) {
        if (drawMode) {
            ctx.lineTo(x, y);
            ctx.stroke();
            if (isRecord) {
                save.push([x, y]);
            }
            if (op === 3){
                endX = x;
                endY = y;
            }
        }
    }

   

    function mouseUp(isRecord) {
        drawMode = false;
        if (isRecord) {
            if(op != 2){
                save.push([]);
                window.localStorage[key] = JSON.stringify(save);
                historyImgs.push(document.getElementById('drawable').toDataURL());
                step = historyImgs.length-1;
                console.log('mouseup'+step); 
            }
        }
    }

    function redo() {
        if (save) {
            for (var i = 0, max = save.length; i < max; i++) {
                if (save[i].length > 2) {
                    mouseDown(save[i][0], save[i][1], save[i][2], save[i][3]);
                } else if (save[i].length === 2) {
                    mouseMove(save[i][0], save[i][1]);
                } else {
                    mouseUp();
                }
            }
        }
    }

   

    (function() {
        $("#size").change(function() {
            currentSize = parseInt($(this).val(), 10);
        });
    })(); //size

    (function() {
        $("#text").click(function() {
            textmode = 1;
        });
    })(); // into text mode

    $(function(){
        $("#drawable").click(function(e){
            if(alreadyinput === 0 && textmode === 1){
                op = 2;
                var div = $('<input id = "test"  type="text" />')
                .css({
                    "left": e.pageX + 'px',
                    "top": e.pageY + 'px', 
                })
                .appendTo(document.body);
                x = e.pageX;
                y = e.pageY;
                alreadyinput = 1;
            }
        });
    });//text

    $(function(){
        $(document.body).click(function(){
            if(alreadyinput === 1 && document.getElementById("test").value != []){
                var inputVal = document.getElementById("test").value;
                console.log(inputVal);
                var selectedfont = $("#fontselect").children("option:selected").val();
                selectedfont = parseInt($(size).val(), 10) + "px " + selectedfont;
                ctx.font = selectedfont;
                console.log(ctx.font);
                ctx.fillStyle = document.getElementById("mycolor").value;
                ctx.fillText(inputVal, x - 220, y-10);
                var el = document.getElementById('test');
                el.remove();
                alreadyinput = 0;
                textmode = 0;
                op = 1;
                $("#drawable").css('cursor','url(source/pen.png),auto');
                historyImgs.push(document.getElementById('drawable').toDataURL());
                console.log(historyImgs.length-1);
                step = historyImgs.length-1;
                console.log(historyImgs.length-1);
                console.log("push image step = " + step);
                
            }
        });
    });//text
    

    (function() {
        $("#clear").click(function() {
            ctx.clearRect(0, 0, $drawable.width(), $drawable.height());
            // ctx.fillStyle = "white";
            // ctx.fillRect(0, 0, drawable.width, drawable.height);
            save = [];
            historyImgs = [];
            historyImgs.push(document.getElementById('drawable').toDataURL());
            step = 0;
            window.localStorage[key] = "";
        });
    })(); //clear

    (function() {
        $("#drawable").click(function() {
            console.log('click');
        });
        $("#drawable").mousemove(function() {
            console.log('move');
        });
    })();




   //circle

    (function() {
        $("#undo").click(function() {
            if(step > 0){
                step--;
                console.log('undo'+step);
                var canvasPic = new Image();
                canvasPic.src = historyImgs[step];
                canvasPic.onload = function () { 
                    var tmp = ctx.globalCompositeOperation;
                    ctx.globalCompositeOperation="source-over";
                    ctx.clearRect(0, 0, $drawable.width(), $drawable.height());
                    ctx.drawImage(canvasPic, 0, 0);
                    ctx.globalCompositeOperation = tmp;
                }
            }  
        });
    })(); //undo

    (function() {
        $("#redo").click(function() {
            console.log('length = '+historyImgs.length,'step=' + step);
            if(step < historyImgs.length-1){
                step++;
                var canvasPic = new Image();
                canvasPic.src = historyImgs[step];
                canvasPic.onload = function () {
                    var tmp = ctx.globalCompositeOperation;
                    ctx.globalCompositeOperation="source-over";
                    ctx.clearRect(0, 0, $drawable.width(), $drawable.height());
                    ctx.drawImage(canvasPic, 0, 0);
                    ctx.globalCompositeOperation = tmp;
                }
            }    
        });
    })(); //redo

    (function() {
        $("#mycolor").click(function() {
            var x = document.getElementById("mycolor").value;
            currentColor = x;
        });
    })();//change color
    (function() {
        $("#pen").click(function() {
            op = 1;
            $("#drawable").css('cursor','url(source/pen.png),auto');
        });
    })();//pen clicked
    (function() {
        $("#eraser").click(function() {
            op = 0;
            $("#drawable").css('cursor','url(source/eraser.png),auto');
        });
    })();
    (function() {
        $("#text").click(function() {
            $("#drawable").css('cursor','url(source/text.png),auto');
            op = 2;
        });
    })();
    (function() {
        $("#rec").click(function() {
            op = 4;
            $("#drawable").css('cursor','url(source/rec.png),auto');
        });
    })();
    (function() {
        $("#tri").click(function() {
            op = 5;
            $("#drawable").css('cursor','url(source/tri.png),auto');
        });
    })();
    (function() {
        $("#circle").click(function() {
            console.log('circle');
            $("#drawable").css('cursor','url(source/circle.png),auto');
            op = 3;
        });
    })();//circle clicked
    

    (function() {
        $("#fill").click(function() {
            if(fill === 1){
                fill = 0;
            }
            else{
                fill = 1;
            } 
        });
    })();
    
    (function() {
        $("#upload").click(function() {
            var img = new Image();
            img.src = URL.createObjectURL(document.getElementById("inp").files[0]);
            img.onload = function () { 
                ctx.clearRect(0, 0, $drawable.width(), $drawable.height());
                ctx.drawImage(img, 0, 0);
                historyImgs.push(document.getElementById('drawable').toDataURL());
                step = historyImgs.length-1;
            } 
        });
    })();//upload
    (function() {
        $("#select").click(function() {
            document.getElementById("inp").click();
        });
    })();

    $($drawable).on('mousedown', function(e) {
        if(op === 3 || op === 4 || op === 5){
            drawingtmp = document.getElementById('drawable').toDataURL();
            scribble_last_mousex = parseInt(e.clientX-scribble_canvasx);
            scribble_last_mousey = parseInt(e.clientY-scribble_canvasy);
            scribble_mousedown = true;
        }
    });
    
    //Mouseup
    $($drawable).on('mouseup', function(e) {
        if(op === 3 || op === 4 || op === 5)
            scribble_mousedown = false;
    });
    

    $("#drawable").on('mousemove', function(e) { 
        scribble_mousex = parseInt(e.clientX-scribble_canvasx);
        scribble_mousey = parseInt(e.clientY-scribble_canvasy);
        if(scribble_mousedown && op === 4) {
            ctx.clearRect(0, 0, $drawable.width(), $drawable.height()); //clear canvas
            var canvasPic = new Image();
            canvasPic.src = drawingtmp;
            ctx.drawImage(canvasPic, 0, 0);
            //Save
            ctx.save();
            ctx.beginPath();
            //Dynamic scaling
            ctx.strokeStyle = document.getElementById("mycolor").value;
            ctx.lineWidth = parseInt($(size).val(), 10);
            ctx.fillStyle = document.getElementById("mycolor").value;
            if (fill) ctx.fillRect(scribble_last_mousex, scribble_last_mousey, scribble_mousex - scribble_last_mousex, scribble_mousey - scribble_last_mousey);
            ctx.rect(scribble_last_mousex, scribble_last_mousey, scribble_mousex - scribble_last_mousex, scribble_mousey - scribble_last_mousey);
            ctx.stroke();
        }
        else if(scribble_mousedown && op === 3) {
            ctx.clearRect(0, 0, $drawable.width(), $drawable.height()); //clear canvas
            var canvasPic = new Image();
            canvasPic.src = drawingtmp;
            ctx.drawImage(canvasPic, 0, 0);
            ctx.save();
            ctx.beginPath();
            var scalex = 1*((scribble_mousex-scribble_last_mousex)/2);
            var scaley = 1*((scribble_mousey-scribble_last_mousey)/2);
            ctx.scale(scalex,scaley);
            var centerx = (scribble_last_mousex/scalex)+1;
            var centery = (scribble_last_mousey/scaley)+1;
            ctx.arc(centerx, centery, 1, 0, 2*Math.PI);
            ctx.restore();
            ctx.strokeStyle = document.getElementById("mycolor").value;
            ctx.fillStyle = document.getElementById("mycolor").value;
            ctx.lineWidth = parseInt($(size).val(), 10);
            if(fill) ctx.fill();
            ctx.stroke();
        }
        else if(scribble_mousedown && op == 5) {
            console.log("triangle");
            ctx.clearRect(0, 0, $drawable.width(), $drawable.height()); //clear canvas
            var canvasPic = new Image();
            canvasPic.src = drawingtmp;
            ctx.drawImage(canvasPic, 0, 0);
            ctx.beginPath();
            ctx.strokeStyle = document.getElementById("mycolor").value;
            ctx.fillStyle = document.getElementById("mycolor").value;
            ctx.lineWidth = parseInt($(size).val(), 10);
            ctx.moveTo((scribble_last_mousex+ scribble_mousex) / 2, scribble_last_mousey);
            ctx.lineTo(scribble_mousex, scribble_mousey);
            ctx.lineTo(scribble_last_mousex, scribble_mousey);
            if(fill) ctx.fill();
            ctx.closePath();
            ctx.stroke();
        }
    });

    
    (function() {
        ctx.lineCap = "round";
        ctx.fillStyle = "white";
        ctx.fillRect(0, 0, $drawable.width(), $drawable.height());
        $drawable.mousedown(function(e) {
            mouseDown(currentColor, currentSize, e.pageX - $drawable.position().left, e.pageY - $drawable.position().top, true);
        }).mousemove(function(e) {
            mouseMove(e.pageX - $drawable.position().left, e.pageY - $drawable.position().top, true);
        }).mouseup(function(e) {
            mouseUp(true);
        });
    })(); //drawable
    redo();
});

